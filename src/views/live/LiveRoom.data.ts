import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
import {JVxeTypes,JVxeColumn} from '/@/components/jeecg/JVxeTable/types'
//列表数据
export const columns: BasicColumn[] = [
   {
    title: '直播间名称',
    align:"center",
    dataIndex: 'name'
   },
   {
    title: '推流域名',
    align:"center",
    dataIndex: 'pushDomain'
   },
   {
    title: '播放域名',
    align:"center",
    dataIndex: 'pullDomain'
   },
   {
    title: '状态',
    align:"center",
    dataIndex: 'status'
   },
   {
    title: '在线人数',
    align:"center",
    dataIndex: 'clients'
   },
   {
    title: '入口带宽',
    align:"center",
    dataIndex: 'recv30s'
   },
   {
    title: '出口带宽',
    align:"center",
    dataIndex: 'send30s'
   },
   {
    title: '视频信息',
    align:"center",
    dataIndex: 'video'
   },
   {
    title: '音频信息',
    align:"center",
    dataIndex: 'audio'
   },
   {
    title: '已发送',
    align:"center",
    dataIndex: 'sendBytes'
   },
   {
    title: '已收到',
    align:"center",
    dataIndex: 'recvBytes'
   },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '直播间名称',
    field: 'name',
    component: 'Input',
  },
  {
    label: '推流域名',
    field: 'pushDomain',
    component: 'Input',
  },
  {
    label: '播放域名',
    field: 'pullDomain',
    component: 'Input',
  },
  {
    label: '状态',
    field: 'status',
    component: 'Input',
  },
  {
    label: '在线人数',
    field: 'clients',
    component: 'Input',
  },
  {
    label: '入口带宽',
    field: 'recv30s',
    component: 'Input',
  },
  {
    label: '出口带宽',
    field: 'send30s',
    component: 'Input',
  },
  {
    label: '视频信息',
    field: 'video',
    component: 'Input',
  },
  {
    label: '音频信息',
    field: 'audio',
    component: 'Input',
  },
  {
    label: '已发送',
    field: 'sendBytes',
    component: 'Input',
  },
  {
    label: '已收到',
    field: 'recvBytes',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
	{
	  label: '',
	  field: 'id',
	  component: 'Input',
	  show: false
	},
];
//子表单数据
//子表表格配置
export const liveClientColumns: JVxeColumn[] = [
    {
      title: '直播间ID',
      key: 'rootId',
      type: JVxeTypes.input,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
    {
      title: 'IP',
      key: 'ip',
      type: JVxeTypes.input,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
    {
      title: '推送',
      key: 'publish',
      type: JVxeTypes.input,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
    {
      title: '在线时长',
      key: 'alive',
      type: JVxeTypes.inputNumber,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
    {
      title: '已发送',
      key: 'sendBytes',
      type: JVxeTypes.inputNumber,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
    {
      title: '已接收',
      key: 'recvBytes',
      type: JVxeTypes.inputNumber,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
    {
      title: 'kbps',
      key: 'kbps',
      type: JVxeTypes.input,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
  ]


// 高级查询数据
export const superQuerySchema = {
  name: {title: '直播间名称',order: 0,view: 'text', type: 'string',},
  pushDomain: {title: '推流域名',order: 1,view: 'text', type: 'string',},
  pullDomain: {title: '播放域名',order: 2,view: 'text', type: 'string',},
  status: {title: '状态',order: 3,view: 'text', type: 'string',},
  clients: {title: '在线人数',order: 4,view: 'text', type: 'string',},
  recv30s: {title: '入口带宽',order: 5,view: 'text', type: 'string',},
  send30s: {title: '出口带宽',order: 6,view: 'text', type: 'string',},
  video: {title: '视频信息',order: 7,view: 'text', type: 'string',},
  audio: {title: '音频信息',order: 8,view: 'text', type: 'string',},
  sendBytes: {title: '已发送',order: 9,view: 'text', type: 'string',},
  recvBytes: {title: '已收到',order: 10,view: 'text', type: 'string',},
  //子表高级查询
  liveClient: {
    title: '客户端',
    view: 'table',
    fields: {
        rootId: {title: '直播间ID',order: 0,view: 'text', type: 'string',},
        ip: {title: 'IP',order: 1,view: 'text', type: 'string',},
        publish: {title: '推送',order: 2,view: 'text', type: 'string',},
        alive: {title: '在线时长',order: 3,view: 'number', type: 'number',},
        sendBytes: {title: '已发送',order: 4,view: 'number', type: 'number',},
        recvBytes: {title: '已接收',order: 5,view: 'number', type: 'number',},
        kbps: {title: 'kbps',order: 6,view: 'text', type: 'string',},
    }
  },
};

/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[]{
// 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}